<?php
require_once "php/UsuarioDAO.php";

if (!isset($_SESSION)) {
    session_start();
}

// tempo de inatividade e verificação de acesso ao arquivo
$tempo = new Usuario();
$tempo->tempo();

if (isset($_POST['ok'])) {

    $dados = new Usuario();
    // Pegar foto para salvar

    $capa = basename($_FILES['capa']['name']);
    $capaTemp = $_FILES['capa']['tmp_name'];
    $capaTipo = $_FILES['capa']['type'];
    $nomeCapa = filter_input(INPUT_POST, 'nome', FILTER_SANITIZE_MAGIC_QUOTES);


    if (empty($capa) && empty($nomeCapa)) {
        echo "<div class='alert alert-danger'>
    <strong>Atenção! </strong>Os campos não podem ser vazios</div>";
    }

    // verifica tipos de imagens
    elseif(preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp)$/", $capaTipo)) {

        $img = utf8_decode($capa);
        $tituloCapa = utf8_decode($nomeCapa);
        // Verifica se o select possui informação
        $dados->setCapa($img);
        $dados->setTitulo($tituloCapa);
        
        // move imagem para uma pasta no servidor        
        @move_uploaded_file($capaTemp, "album/capa/" . $dados->getCapa());
        
        $dados->capas($dados);


    }else{
       echo "<div class='alert alert-danger'>
    <strong>Atenção! </strong>Você só pode gravar imagem do tipo <strong>jpg, jpeg, png, gif e bmp</strong></div>";
    }    

    unset($imagem,$titulo,$dados,$tipofoto,$temp,$tipoAlbum,$tipoAlbumDigitado);
    
}

?>

<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" type="image/png" href="imagens/studio.png">
        <link rel="stylesheet" href="css/jquery-ui.css" />
        <script src="javascript/jquery-1.8.2.js"></script>
        <script src="javascript/jquery-ui.js"></script>
        <script src="javascript/jquery.min.js"></script>


        <script>
        // Verificação de campos vazios
            function galeriaFoto(){
             if (document.formulario.foto.value == "" || document.formulario.nome.value == "") {
                alert("Os campos não podem ser nulos");
                return false;
                }
            }
        </script>

        <title>Cadastro de Capas</title>

        <!-- CSS -->
      <!-- CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- IE10 desktop Windows 8 -->
        <link href="css/ie10-viewport-bug-workaround.css" rel="stylesheet">

        <link href="css/jumbotron.css" rel="stylesheet">
        <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
        <script src="css/ie-emulation-modes-warning.js"></script>

         <!--[if lt IE 9]>
         <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
         <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
         <![endif]-->

        <style>

            h4{
                font-family: "Times New Roman", Times, serif;
                color: #23527c;
            }

            #texto-imagem{
                font-family: "Times New Roman", Times, serif;
                color: #5a8393;
            }

            #albuns{
                width: 100%;
                height: 30px;
                border-radius: 5px;
            }

            strong{
                color: #23527c;
            }

            #botaoAlbum{
                margin-top: -97px;
                margin-left: 167px;
                position: relative;
            }

        </style>

    </head>

    <body>

        <!-- Menu -->
        <nav class="navbar navbar-inverse navbar-fixed-top">

            <div class="container">
                <div class="navbar-header">
                 <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <a href="http://www.artebeleza.esy.es/Logout"><img src="imagens/logout.png" width="20" height="20" alt="logout"/></a>
                    </button>
                    <a class="navbar-brand" href="http://www.artebeleza.esy.es/principal">Principal</a>
                    <a class="navbar-brand" href="http://www.artebeleza.esy.es/galeria-gravar">Gravar Galeria</a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <div class="form-group">
                        <div class="navbar-form navbar-right">   
                            <a href="http://www.artebeleza.esy.es/Logout"><img src="imagens/logout.png" width="32" height="32" alt="logout"/></a>  
                        </div>
                    </div>
               </div>
            </div>
        </nav>


        <div class="jumbotron">
            <div class="container">

            <h2 id="texto-imagem">Salvar imagens</h2>

                <!-- Formulário de Galeria -->
                <form method="POST" name="formulario" enctype="multipart/form-data" onsubmit="return galeriaFoto()">

                <div class="form-group">
                        <label for="nome"><h4>*Nome da Capa</h4></label>
                        <input type="text" class="form-control" name="nome" id="nome" placeholder="Digite o nome da capa" />
                </div>

                <div class="form-group">
                        <label for="foto"><h4>*Foto Capa</h4></label>
                        <input type="file" class="form-control" name="capa" id="foto" />
                </div>


                        <button type="submit" name="ok" class="btn btn-success" >Cadastrar</button>
                        <button type="reset" class="btn btn-danger">Limpar</button>
             </form>
            
            </div>  
            
        </div>

    </body>
</html>         

