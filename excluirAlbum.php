<?php
require_once "php/UsuarioDAO.php";
if (!isset($_SESSION)) {
    session_start();
}
// tempo de inatividade e verificação de acesso ao arquivo
$tempo = new Usuario();
$tempo->tempo();

if (isset($_POST['ok'])) {

    $dados = new Usuario();
    // Pegar foto para salvar

    $album = filter_input(INPUT_POST, "album", FILTER_SANITIZE_MAGIC_QUOTES);

    $dados->setAlbum($album);

    $dados->excluirAlbum($dados);

    unset($dados,$album);

} 

?>

<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" type="image/png" href="imagens/studio.png">
        <link rel="stylesheet" href="http://code.jquery.com/ui/1.9.0/themes/base/jquery-ui.css" />
        <link rel="shortcut icon" type="image/png" href="imagens/studio.png">
        <script src="http://code.jquery.com/jquery-1.8.2.js"></script>
        <script src="http://code.jquery.com/ui/1.9.0/jquery-ui.js"></script>
        <script>
        // Verificação de campos vazios
            function galeriaFoto(){
             if (document.formulario.tituloFoto.value == "" || document.formulario.foto.value == "") {
                alert("Os campos não podem ser nulos");
                return false;
                }
            }
        </script>

        <title>Exclusão de Albuns</title>

      <!-- CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- IE10 desktop Windows 8 -->
        <link href="css/ie10-viewport-bug-workaround.css" rel="stylesheet">

        <link href="css/jumbotron.css" rel="stylesheet">
        <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
        <script src="css/ie-emulation-modes-warning.js"></script>

         <!--[if lt IE 9]>
         <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
         <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
         <![endif]-->

        <style>

            h4{
                font-family: "Times New Roman", Times, serif;
                color: #23527c;
            }

            #texto-imagem{
                font-family: "Times New Roman", Times, serif;
                color: #5a8393;
            }

            #albuns{
                width: 100%;
                height: 30px;
                border-radius: 5px;
            }

            strong{
                color: #23527c;
            }

        </style>

    </head>

    <body>

        <!-- Menu -->
        <nav class="navbar navbar-inverse navbar-fixed-top">

            <div class="container">
                <div class="navbar-header">
                 <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <a href="http://www.artebeleza.esy.es/Logout"><img src="imagens/logout.png" width="20" height="20" alt="logout"/></a>
                    </button>
                    <a class="navbar-brand" href="http://www.artebeleza.esy.es/principal">Principal</a>
                    <a class="navbar-brand" href="http://www.artebeleza.esy.es/galeria-gravar">Galeria Gravar</a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <div class="form-group">
                        <div class="navbar-form navbar-right">   
                            <a href="http://www.artebeleza.esy.es/Logout"><img src="imagens/logout.png" width="32" height="32" alt="logout"/></a>  
                        </div>
                    </div>
               </div>
            </div>
        </nav>


        <div class="jumbotron">
            <div class="container">

            <h2 id="texto-imagem">Excluir Albuns</h2>

                <!-- Formulário de Galeria -->
                <form method="POST" action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>">
        

                <div class="form-group">

                 <h4>Categoria do Album</h4>
                <strong>Selecionar pasta existente</strong>
                <select name="album" id="albuns">

                <?php 
                    
                // Objeto do método de buscar nome de albuns.    
                $buscaAlbuns = new Usuario();

                $buscaAlbuns->albunsDeFotos();

                unset($buscaAlbuns);

                ?>    

                </select>
                </div>

                        <button type="submit" name="ok" class="btn btn-danger" >Excluir</button>

             </form>
            </div>  
            
        </div>

    </body>
</html>