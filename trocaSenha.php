<?php
require_once 'php/UsuarioDAO.php';
if (!isset($_SESSION)) {
    session_start();
}
$tempo = new Usuario();
$tempo->tempo();

if (isset($_POST['ok'])){

    $trocarSenha = new Usuario();
    // Pega dados
    $senha_antiga = filter_input(INPUT_POST, "senhaAntiga", FILTER_SANITIZE_MAGIC_QUOTES);
    $senha_nova = filter_input(INPUT_POST, "senhaNova", FILTER_SANITIZE_MAGIC_QUOTES);
    $senha_novamente = filter_input(INPUT_POST, "senhaNovamente", FILTER_SANITIZE_MAGIC_QUOTES);
    // Compara se a senha são indenticas.
    if ($senha_nova === $senha_novamente) {
        
       $trocarSenha->trocarSenha(sha1($senha_antiga),$senha_novamente);
              
    }else{
        echo '<div class="alert alert-danger">
            <strong>Atenção!</strong> As senhas digitada não são iguais</div>';
    }

}

    unset($trocarSenha,$senha_antiga,$senha_nova,$senha_novamente);

?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" type="image/png" href="imagens/studio.png">
        <script src="http://code.jquery.com/jquery-1.8.2.js"></script>
        
               <!-- CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- IE10 desktop Windows 8 -->
        <link href="css/ie10-viewport-bug-workaround.css" rel="stylesheet">

        <link href="css/jumbotron.css" rel="stylesheet">

        <script src="css/ie-emulation-modes-warning.js"></script>

        <title>Trocar Senha</title>
        <style>
             h3{
                font-family: "Times New Roman", Times, serif;
                color: #23527c;
            }

            h4{
                font-family: "Times New Roman", Times, serif;
                color: #23527c;
            }
        </style>
        
        <script>
        
         //tecla ENTER
            $(document).ready(function(){
                /* ao pressionar uma tecla em um campo que seja de class="inputUnico", 
                em vez de submeter o formulário, vai pular para o próximo campo.
                o formulário só vai executar o submit com a tecla enter se estiver no ultimo campo do formulário*/
                $('.form-control').keypress(function(e){
                    /* 
                     * verifica se o evento é Keycode (para IE e outros browsers)
                     * se não for pega o evento Which (Firefox)
                    */
                   var tecla = (e.keyCode?e.keyCode:e.which);
                    
                   /* verifica se a tecla pressionada é a tecla ENTER */
                   if(tecla == 13){
                       /* guarda o seletor do campo onde foi pressionado Enter */
                       campo =  $('.form-control');
                       /* pega o indice do elemento*/
                       indice = campo.index(this);
                       /*soma mais um ao indice e verifica se não é null
                        *se não for é porque existe outro elemento
                       */
                      if(campo[indice+1] != null){
                         /* adiciona mais 1 no valor do indice */
                         proximo = campo[indice + 1];
                         /* passa o foco para o proximo elemento */
                         proximo.focus();
                      }else{
                        return true;
                      }
                   }
                   if(tecla == 13){
                    /* impede o submit caso esteja dentro de um form */
                    e.preventDefault(e);
                    return false;
                    }else{
                    /* se não for tecla enter deixa escrever */
                    return true;
                    }
                })
             })

        </script>
       
    </head>
    <body>
        
         <!-- Menu -->
        <nav class="navbar navbar-inverse navbar-fixed-top">

            <div class="container">
                <div class="navbar-header">
                    <a class="navbar-brand" href="http://www.artebeleza.esy.es/trocaSenha">Troca Senha</a>
                    <a class="navbar-brand" href="http://www.artebeleza.esy.es/principal">Principal</a>
                </div>

            </div>
        </nav>
    
         
         <div class="jumbotron">
                <div class="container">
                
                    <!-- Formulário -->
                    <form method="POST" action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>">
                        
                        <div class="form-group">
                            <label for="senhaAntiga"><h4>*Senha Antiga</h4></label>
                            <input type="password" class="form-control" name="senhaAntiga" id="senhaAntiga" placeholder="Digite sua senha antiga">
                        </div>
                        
                         <div class="form-group">
                            <label for="senhaNova"><h4>*Nova Senha</h4></label>
                            <input type="password" class="form-control" name="senhaNova" id="senhaNova" placeholder="Digite sua nova senha">
                        </div>
                        
                         <div class="form-group">
                            <label for="senhaNovamente"><h4>*Digite Novamente</h4></label>
                            <input type="password" class="form-control" name="senhaNovamente" id="senhaNovamente" placeholder="Digite novamente a senha nova">
                        </div>
                        
                        <button type="submit" name="ok" class="btn btn-success">Alterar Senha</button>
                        <button type="reset" class="btn btn-danger">Limpar</button>
                        
                    </form>
                    
                </div>
             
         </div>
        
    </body>
</html>
