
<?php
require_once "php/UsuarioDAO.php";

if (isset($_POST['ok'])):
    $dados = new Usuario();

    $login = filter_input(INPUT_POST, "txtEmail", FILTER_SANITIZE_EMAIL);
    $senha = filter_input(INPUT_POST, "txtSenha", FILTER_SANITIZE_MAGIC_QUOTES);

    if (!filter_var($login, FILTER_VALIDATE_EMAIL) === FALSE) {
    $dados->setLogin($login);
    $dados->setSenha($senha);
    $dados->logar($dados);   

    }else{
        echo '<div class="alert alert-danger">
  <strong>Atenção! </strong>Endereço de Email invalido</div>';;
    }

    unset($login,$senha,$dados);

    
endif;
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="Fernando Heitor">
        <link rel="shortcut icon" type="image/png" href="imagens/studio.png">
       
        <title>Arte Beleza</title>

        <!-- IE10 desktop Windows 8 -->
        <link href="css/ie10-viewport-bug-workaround.css" rel="stylesheet">

        <link href="css/jumbotron.css" rel="stylesheet">
        <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
        <script src="css/ie-emulation-modes-warning.js"></script>

         <!--[if lt IE 9]>
         <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
         <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
         <![endif]-->

        <!-- CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
    </head>

    <style>
          h1,p,h2{
                text-align: center;
            } 
            img{
                width: 100%;
                height: 469px;
                margin-left: auto;
            }

            table,th{
                text-align: center;
                
            }

            .btn-default{
                background: #5bc0de;
                color: white;
                font-size: 16px;
            }

            .btn-default:hover{
                background: lightblue;
            }

            .foto{
                width: 50px;
                height: 50px;
            }

            #organizar{
                margin-top: 45px;
            }
    </style>

    <body>

   
        <!-- Menu -->
        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Menu</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <a class="navbar-brand" href="http://www.artebeleza.esy.es/">Arte Beleza</a>

                </div>

                <div id="navbar" class="navbar-collapse collapse">

                    <!-- Formulário de Login -->      
                    <form class="navbar-form navbar-right" method="POST" action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>">

                        <div class="form-group">
                            <input type="text" placeholder="E-mail" name="txtEmail" class="form-control" title="Somente para funcionários" required>
                        </div>
                        <div class="form-group">
                            <input type="password" placeholder="Senha" name="txtSenha" class="form-control" title="Somente para funcionários" required>
                        </div>
                        <button type="submit" class="btn btn-primary" name = "ok">Logar</button>
                        <a href=""><button class="btn btn-danger" onclick="esqueciSenha()">Esqueci minha senha</button></a>

                    </form>
                </div>
            </div>
        </nav>

        <!-- Inicio de apresentação -->

        <img src="imagens/brush.jpg" alt="perfil"/>


        <div class="jumbotron">

            <div class="container">
                <h1>Bem Vindo</h1>
                <p>Comece a semana ainda mais linda.</p>
                <p>Agende seu horário</p>
                <p><a class="btn btn-info btn-lg" href="http://www.artebeleza.esy.es/cadastro" title="Agendamento" role="button">Agendar &raquo;</a></p>
            </div>
        </div>

        <div class="container">
    
        <!-- Serviços -->

<div class="row">
    
      <div class="col-md-4">
             
        <table class="table table-bordered">
    <thead>
      <tr>
        <th>Maquiagem</th>
        <th>Valor</th>
        </tr>
    </thead>
    <tbody>
      <tr>
        <td>Individual</td>
        <td>R$ 100,00</td>
        </tr>
        <tr>
        <td>Noivinha</td>
        <td>R$ 30,00</td>
        </tr>
        <tr>
        <td>Noiva/Debutante + Acompanhantes</td>
        <td>R$ 175,00 (R$ 75,00 cada Acompanhantes)</td>
        </tr>
        <tr>
        <td>Debutante</td>
        <td>R$ 90,00</td>
        </tr>
        
    </tbody>
  </table>


        </div>
     
       <div class="col-md-4" id="organizar">
        
            <h2>Design de Sobrancelha</h2>
                
            <p>Valoriza o desenho da sua sobrancelha à deixando mais bonita de acordo com o formato do seu rosto. <br/>Valor: Simples <strong>R$ 15,00</strong>. Com hena <strong>R$ 25,00</strong></p>        
            </div>

        <div class="col-md-4" id="organizar">
        
        <h2>Galeria</h2>
        
        <p>Veja algumas fotos do trabalho</p>
        <p><a class="btn btn-default" href="http://www.artebeleza.esy.es/galeria" role="button">Ver Galeria &raquo;</a></p>
        
        </div>

</div>
  <hr>

  <!-- Rodapé -->
            <footer>
                <p>
                <a href="https://www.facebook.com/mariastella.gomessoares" target="_blank"><img src="imagens/facebook.png" class="foto" title="Facebook" /></a>&nbsp;
                <a href="https://www.instagram.com/stella_gomes16" target="_blank"><img src="imagens/instagram.png" class="foto" title="Instagram" /></a>
                </p>
            </footer>

        <!-- Versão dispositivos móveis para login -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="css/jquery.min.js"><\/script>')</script>
        <script src="css/bootstrap.min.js"></script>
        <!-- desktop Windows 8  -->
        <script src="css/ie10-viewport-bug-workaround.js"></script>
        <!-- <script src="javascript/jquery-1.8.2.js"></script> -->
        <script src="javascript/apresentacao.js"></script>


    </body>
</html>
