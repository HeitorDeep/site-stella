<?php
require_once 'php/UsuarioDAO.php';
if (!isset($_SESSION)) {
    session_start();
}
$usuario = new Usuario();
$usuario->tempo();

?>
<!DOCTYPE html>
<html lang="pt-br">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" type="image/png" href="imagens/studio.png">

    <title>Exclusão Imagens</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- CSS -->
    <link href="css/2-col-portfolio.css" rel="stylesheet">

    <!-- HTML5 Shim  -->
   
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<style>
/*Controle de tamanho da imagem*/
img{
    width: 100%;
    height: 369px;
    margin-left: auto;
}
/*Passar o mouse em cima do link*/
a:hover{
    background: #a6b3d1;
    border-radius: 5px;
    text-decoration:none;
}
/*Link clicado*/
a:visited{
    color: #6d9bba;
    text-decoration: none;
}

#titulo{
    font-size: 16px;
    text-align: center;
    color: #6d9bba;
    font-weight: bold;
}

</style>
</head>

<body>

    <!-- Navegação -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Menu -->
            <div class="navbar-header">
                <a class="navbar-brand" href="http://www.artebeleza.esy.es/principal-galeria">Arte Beleza</a>
            </div>
         </div>
    </nav>

    <!-- Principal conteúdo -->
    <div class="container">

        <div class="row">
            <div class="col-lg-12">
            <!-- Titulo -->
                <h1 class="page-header">
                    <small>Exclusão de Imagens</small>
                </h1>
            </div>
        </div>

        <!-- Imagens -->
        <div class="row">

 <?php
// Objeto do método de buscar foto para exclusão.
 $buscarFotos = new Usuario();

 $buscarFotos->buscafotoExclusao();

 unset($buscarFotos);

 ?>
</div>
    
     <!-- jQuery -->
    <script src="javascript/jquery.js"></script>

    <!-- Bootstrap -->
    <script src="javascript/bootstrap.min.js"></script>

</body>

</html>

