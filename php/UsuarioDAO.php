<?php

if (!isset($_SESSION)) {
    session_start();
}
?>
<?php

require_once dirname(__FILE__) . "/Conexao.php";
require_once dirname(__FILE__) . "/ControleDeDados.php";
require_once dirname(__FILE__) . "/ControleInterface.php";

class Usuario extends ControleDeDados implements ControleInterface {


// tempo de inatividade
public function tempo(){

    // registro de tempo que logou
$limite = isset($_SESSION['tempoLimite']) ? $_SESSION['tempoLimite'] : "";
$registro = isset($_SESSION['registro']) ? $_SESSION['registro'] : "";

if ($registro):
    // Tempo menos o registro
    $segundos = time() - $registro;

endif;

// Verifica que o tempo inativo é maior que o limite de 900.
if ($segundos > $limite):
    session_destroy();
    echo '<div class="alert alert-danger">
    <strong>Atenção! </strong>Sua seção expirou! <a href="http://www.artebeleza.esy.es/">Por favor, logar novamente</a></div>';


else:
    $_SESSION['registro'] = time();
endif;

//Caso o usuário não esteja autenticado, limpa os dados e redireciona
if (!isset($_SESSION['nome']) || !isset ($_SESSION['senha'])):
    // Destruir sessão
    session_destroy();
    // Limpa a session
    unset($_SESSION['nome']);
    unset($_SESSION['senha']);

    header("Location: http://www.artebeleza.esy.es/");
endif;

}

    // Método de Logar no sistema
public function logar(ControleDeDados $logar){

    // Abre conexão
     $pdo = Conexao::conectar();
    try {

        // Pesquisa se o login existe.
        $login = $pdo->prepare("SELECT ID_USUARIO,NOME,EMAIL,SENHA FROM usuario WHERE EMAIL = ? LIMIT 4");

        $login->bindValue(1, $logar->getLogin());
        $login->execute();
        // Pega o resultado dessa consulta.
        $resultado = $login->fetch(PDO::FETCH_OBJ); 

        $login->closeCursor();
        // Verifica se login existe
        if ($login->rowCount() == 1) {
            
            // Pega informações úteis do banco, ID e Senha
            $id = (int) htmlentities($resultado->ID_USUARIO);
            $senha = htmlentities($resultado->SENHA);
            
            // Verifica se o ID do usuário está bloqueado
            if ($this->checkBrute($id)) {
                
                return false;
                
            }else{

                // Caso ao contrario compara se as senhas digitadas são iguais a do banco
                if ($senha == $this->getSenha()) {

                    //Limite de tempo de inatividade
                    date_default_timezone_set("Brazil/East");
                    $limiteTempo = 900;

                    // Hora logada
                    $_SESSION['registro'] = time();
                    $_SESSION['tempoLimite'] = $limiteTempo;

                    $_SESSION['nome'] = htmlentities($resultado->NOME); 
                    $_SESSION['email'] = htmlentities($resultado->EMAIL);
                    $_SESSION['senha'] = htmlentities($resultado->SENHA);

                    header("Location: http://www.artebeleza.esy.es/principal");

                }else{
                // Caso o login esteja certo mas a senha não, grava o tempo e o Id do usuário no banco
                    $now = time();
                    $ip = getenv("REMOTE_ADDR");
                    $hostname = gethostbyaddr($_SERVER['REMOTE_ADDR']);    
                    $block = $pdo->prepare("INSERT INTO tentativas (ID_USUARIO,TEMPO,IP,HOST_NAME) VALUES (?,?,?,?)");
                    $block->bindValue(1, $id);
                    $block->bindValue(2, $now);
                    $block->bindValue(3, $ip);
                    $block->bindValue(4, $hostname);
                    $block->execute();
                    $block->closeCursor();

                    echo '<div class="alert alert-danger"><strong>Atenção! </strong>Senha incorreta</div>';
                    unset($now,$ip,$block,$login,$hostname);
                }

            }

        }else{
            // Login não existe
            echo '<div class="alert alert-danger">
  <strong>Atenção! </strong>Login ou senha invalida.</div>';
        }
        
    } catch (PDOException $e) {
        echo "<div class='alert alert-danger'>
    <strong>Erro: </strong>Ao logar no sistema. Tente novamente mais tarde</div>";
        mail("heitor.aguia@gmail.com", "Erro de logar", $e->getMessage());
    }

    unset($login,$senha,$pdo);
    
}

// Verificação de tentativas de login
public function checkBrute($id){

 $pdo = Conexao::conectar();

$now = time();
// Tempo de 1 hora bloqueado
$validarTempo = $now - (1 * 60 * 60);

// Verifica se o tempo cadastrado é maior que o tempo adquirido
$stmt = $pdo->prepare("SELECT TEMPO FROM tentativas WHERE ID_USUARIO = ? AND TEMPO > ?");
$stmt->bindValue(1, $id);
$stmt->bindValue(2, $validarTempo);
$stmt->execute();

// Verifica tentativas de Id
if ($stmt->rowCount() >= 3) {

        echo '<div class="alert alert-danger">
  <strong>Atenção! </strong>Email esta bloqueado</div>';

  mail("heitor.aguia@gmail.com", "Tentativas de Invasão", "Sua conta foi bloqueada com maximo de tentativas");

  return true;
}else{
    return false;
}

$stmt->closeCursor();
unset($now,$validarTempo,$stmt,$pdo);
}

    // Função Cadastrar
    public function cadastrar(ControleDeDados $cadastrarDados) {

        try {

             $pdo = Conexao::conectar();
            // Verifica se possui data e horario agendado
            $buscarDados = $pdo->prepare("SELECT DATA,HORA FROM cadastro WHERE DATA = ? AND HORA = ? LIMIT 2");

            $buscarDados->bindValue(1, $cadastrarDados->getData());
            $buscarDados->bindValue(2, $cadastrarDados->getHora());

            $buscarDados->execute();

            if ($buscarDados->rowCount() == 1):

                echo "<div class='alert alert-danger'>
    <strong>O horário agendado nesta data já possui horário das {$this->getHora()} "
                . ", verifique novamente outro horário.</strong></div>";
                $buscarDados->closeCursor();
                unset($buscarDados,$pdo);
            else:

                // Inserir dados na tabela cadastro (Agendamento)
                
                $gravar = $pdo->prepare("INSERT INTO cadastro (DATA,HORA,NOME_CLIENTE,TELEFONE,CELULAR,ENDERECO,ACOMPANHANTE,RESULTADO) VALUES (?,?,?,?,?,?,?,?)");

                $gravar->bindValue(1, $cadastrarDados->getData());
                $gravar->bindValue(2, $cadastrarDados->getHora());
                $gravar->bindValue(3, $cadastrarDados->getNome());
                $gravar->bindValue(4, $cadastrarDados->getTelefone());
                $gravar->bindValue(5, $cadastrarDados->getCelular());
                $gravar->bindValue(6, $cadastrarDados->getEndereco());
                $gravar->bindValue(7, $cadastrarDados->getAcompanhante());
                $gravar->bindValue(8, $cadastrarDados->getResultado());

                if ($gravar->execute()) {
                    echo '<div class="alert alert-success">
    <strong>Cadastro Efetuado com Sucesso</strong><br>Após o agendamento, aguardar o contato. Obrigado</div>';
        
                }else{

                echo '<div class="alert alert-danger">
    <strong>Erro ao cadastrar agendamento, verifique se todas as opções estão marcadas</strong></div>';
               
    }

    // Envio de email para o usuário, referente ao agendamento 
    mail("stellagomes06@gmail.com", "Cadastro de Cliente", "Nome: " . $cadastrarDados->getNome() . "\nTelefone: " . 
            $cadastrarDados->getTelefone() . "\nCelular: " . $cadastrarDados->getCelular() . "\nHora: " . $cadastrarDados->getHora() . 
            "\nEndereço: " . $cadastrarDados->getEndereco());
    
                $gravar->closeCursor();

                unset($gravar,$pdo,$cadastrarDados);

            endif;
        } catch (PDOException $ex) {
            echo "<div class='alert alert-danger'>
    <strong>Erro ao gravar os dados, por favor tente mais tarde</strong></div>";
    mail("heitor.aguia@gmail.com", "Erro no Cadastro", $ex->getMessage());
        }
    }

    // Método de excluir agendamento
    public function excluir(ControleDeDados $deletar) {


        try {
        
         $pdo = Conexao::conectar();

        $excluir = $pdo->prepare("DELETE FROM cadastro WHERE ID_CADASTRO = ?");

        $excluir->bindValue(1, $deletar->getId());

        $excluir->execute();
        $excluir->closeCursor();
        header("Location: http://www.artebeleza.esy.es/principal");
        
        unset($deletar,$pdo,$excluir);

     }   catch (PDOException $e) {
           echo "<div class='alert alert-danger'>
    <strong>Erro: </strong>Ao excluir os dados do cliente</div>";
           mail("heitor.aguia@gmail.com", "Erro ao excluir clientes", $e->getMessage());  
        }
    }

    // Método de buscar dados de todos os agendamentos
    public function buscaDados() {

        
        try {
                // Abre conexão.
             $pdo = Conexao::conectar();

             // Paginação do conteúdo.
            $quantidade = 3;
            $pagina = (isset($_GET['pagina'])) ? (int)$_GET['pagina'] : 1;

            $inicio = ($quantidade * $pagina) - $quantidade;
                // Select de duas tabelas (Cadastro e Valores) onde fica armazenado os cadastros.
            $buscaDados = $pdo->query("SELECT cadastro.*,valores.SERVICOS,valores.VALORES,valores.ID_VALORES FROM cadastro JOIN valores ON valores.ID_VALORES = cadastro.RESULTADO ORDER BY DATA ASC, HORA ASC LIMIT $inicio,$quantidade");

            // Verifica se existe dados no banco.
            if ($buscaDados->rowCount() > 0) {

            // Busca todos os dados da tabela.
            while ($linha = $buscaDados->fetch(PDO::FETCH_ASSOC)) {

                /* 
                    Validação dos acompanhantes. Caso obter 0 acompanhantes somar valores * 1
                    Caso acompanhantes obter 1 então faça valores * 2 e caso acompanhante obter
                    mais do que um, fazer soma de valores * acompanhantes.
                */
                $soma = 0;
                $soma = $linha['ACOMPANHANTE'] == 0 ? $linha['VALORES'] * 1: $soma;
                $soma = $linha['ACOMPANHANTE'] == 1 ? $linha['VALORES'] * 2: $soma;
                $soma = $linha['ACOMPANHANTE'] > 1 ? $linha['VALORES'] * $linha['ACOMPANHANTE']: $soma;
                $soma = $linha['VALORES'] == 175 ?  $soma = (75 * $linha['ACOMPANHANTE']) + $linha['VALORES'] : $soma; 
            
                // Caso obter 0 de acompanhantes aprensetar mensagem de não à acompanhantes se não retorna acompanhantes.
                $acomp = '';
                $acomp = $linha['ACOMPANHANTE'] == 0 ? $acomp = 'Não à acompanhantes' : $acomp = $linha['ACOMPANHANTE'];

                // Formatar data BR
                $data = date('d/m/Y', strtotime($linha['DATA']));
                // Dados de apresentação.
                echo '<tr><td class="resultado">' . htmlentities($linha['NOME_CLIENTE']) . '</td>';
                echo '<td class="resultado">' . htmlentities($data) . '</td>';
                echo '<td class="resultado">' . htmlentities($linha['HORA']) . '</td>';
                echo '<td class="resultado">' . htmlentities($linha['ENDERECO']) . '</td>';
                echo '<td class="resultado">' . htmlentities($linha['TELEFONE']) . '</td>';
                echo '<td class="resultado">' . htmlentities($linha['CELULAR']) . '</td>';
                echo '<td class="resultado">' .  htmlentities($linha['SERVICOS']) .'</td>';
                echo '<td class="resultado">' .  htmlentities($acomp) .'</td>';
                echo '<td class="resultado">R$ ' . htmlentities($soma) . '</td>';
                echo "<td id='botao'><a href='php/editar.php?id=" . htmlentities($linha['ID_CADASTRO']) . "'><button type='button' class='btn btn-success btn-sm'>Editar</button></a><p><p> ";
                echo "<a href='php/excluir.php?id=" . htmlentities($linha['ID_CADASTRO']) . "'><button type='button' class='btn btn-danger btn-sm'>Excluir</button></a></td></tr>";
                
            }

            // Paginação
            $qt = $pdo->query("SELECT ID_CADASTRO FROM cadastro");
            // Quantidade do select.
            $numTotal = $qt->rowCount();
            // Quantidade (3) dividido por número total do select
            $totalPagina = ceil($numTotal/$quantidade);

            $exibir = 3;
            /* Se a página anterior for igual a 0 retorna para a 1 se não 
               retorna a anterior*/
            $anterior = (($pagina - 1) == 0) ? 1 : $pagina -1;
            /* Se a proxima página for que o total da soma(quantidade e do select) 
               recebe total de páginas, se não página recebe mais um */
            $posterior = (($pagina + 1) >= $totalPagina) ? $totalPagina : $pagina +1;

            echo '<ul class="pagination">';
            // Páginas anteriores
            for ($i = $pagina - $exibir; $i <= $pagina - 1; $i++) { 
                if ($i > 0) {
                   echo '<li><a href="?pagina='. $i . '">' . $i . '</a></li>';
                }
            }

            echo '<li><a href="?pagina='. $pagina . '">' . $pagina . '</li>';

            // Páginas seguintes
            for ($i = $pagina + 1; $i < $pagina + $exibir ; $i++) { 
                if ($i <= $totalPagina) {
                    echo '<li><a href="?pagina='. $i . '">' . $i . '</a></li>';
                }
            }

            // Últimos dados restantes
             echo "<li><a href=\"?pagina=$totalPagina\">Última</a></li></ul>";


          }else{
            echo "<div class='alert alert-info'>
    <strong>Você não possui clientes no momento</strong></div>";
          }  

        } catch (PDOException $e) {
            echo "<div class='alert alert-danger'>
    <strong>Atenção! </strong>Erro na busca de dados dos clientes</div>";
    mail("heitor.aguia@gmail.com", "Erro na busca de dados dos clientes", $e->getMessage());
        }
        
        unset($buscaDados,$data,$linha,$pdo,$qt,$totalPagina,$numTotal,$soma,$acomp);
    }

    // Método de editar os agendamentos
    public function editar(ControleDeDados $editar) {

         $pdo = Conexao::conectar();
        try {

            $editarClientes = $pdo->prepare("UPDATE cadastro SET DATA = ?, HORA = ? WHERE ID_CADASTRO = ?");

            $editarClientes->bindValue(1, $editar->getData());
            $editarClientes->bindValue(2, $editar->getHora());
            $editarClientes->bindValue(3, $editar->getId());

            $editarClientes->execute();
            $editarClientes->closeCursor();

            unset($editarClientes,$pdo,$editar);
        
            header("Location: http://www.artebeleza.esy.es/principal");

        } catch (PDOException $ex) {
            echo "<div class='alert alert-danger'>
    <strong>Atenção! </strong>Erro ao atualizar os dados</div>";
    mail("heitor.aguia@gmail.com", "Erro na atualização do cliente", $ex->getMessage());
        }
    }

// Método para buscar Horas para o select
    public function buscaSelect(){

         $pdo = Conexao::conectar();
                 try {
                     
                    $buscarSelect = $pdo->query("SELECT HORA FROM hora ORDER BY HORA");
                                        
                    while ($linhas = $buscarSelect->fetch(PDO::FETCH_ASSOC)){   
                        echo "<option value='{$linhas['HORA']}'> " . htmlentities($linhas['HORA']) . "</option>";
                    }
                     
                 } catch (PDOException $ex) {
                    echo "<div class='alert alert-danger'>
    <strong>Erro: </strong>Ao buscar as informações do banco de dados</div>";
            mail("heitor.aguia@gmail.com", "Erro na seleção do select", "Ocorreu um erro na busca dos dados do select " . $ex->getMessage()); 
            
                 }
                 $buscarSelect->closeCursor();
                 unset($pdo,$buscarSelect,$linhas);
    }

    public function indentificacao(ControleDeDados $indentificacao){

         $pdo = Conexao::conectar();
        try {

        $inserir = $pdo->prepare("INSERT INTO tentativas (IP,HOST_NAME) VALUES (?,?)");
        $inserir->bindValue(1, $indentificacao->getIp());
        $inserir->bindValue(2, $indentificacao->getHostName());
        $inserir->execute();

            
        } catch (PDOException $e) {
    mail("heitor.aguia@gmail.com", "Erro na seleção do select", "Ocorreu um erro ao inserir o ip e hostname. ERRO: " . $e->getMessage()
         . "\nIP: " . $this->getIp() . "\nHost-Name: " . $this->getHostName());
    
        }

        unset($inserir,$indentificacao,$pdo);

    }

    // Salvar imagem e titulo na galeria 
    public function galeria(ControleDeDados $imagem){

        try {

             $pdo = Conexao::conectar();
            // Realiza pesquisa para verificar se existe a mesma imagem no banco.
            $verifica = $pdo->prepare("SELECT NOME_FOTO FROM fotos WHERE NOME_FOTO = ?");
            $verifica->bindValue(1, $imagem->getImagem());
            $verifica->execute();
            $verifica->closeCursor();

            if ($verifica->rowCount() == 1) {
                echo '<div class="alert alert-danger">
    <strong>Status: </strong>A foto <strong>'.$imagem->getImagem().'</strong> nome da foto já possui cadastro</div>';

            }else{

            // Insere os dados da imagem e titulo da foto.
            $salvarGaleria = $pdo->prepare("INSERT INTO fotos (NOME_FOTO,TITULO,ALBUM) VALUES (?,?,?)");
            $salvarGaleria->bindValue(1, $imagem->getImagem());
            $salvarGaleria->bindValue(2, $imagem->getTitulo());
            $salvarGaleria->bindValue(3, $imagem->getAlbum());

            $salvarGaleria->closeCursor();

            if ($salvarGaleria->execute()) {
                echo "<div class='alert alert-success'>
    <strong>Status: </strong>Gravado com Sucesso</div>";

            }else{
                echo "<div class='alert alert-danger'>
    <strong>Status: </strong>Erro ao gravar a imagem</div>";
            }

            unset($pdo,$salvarGaleria,$verifica,$imagem);
}        
        } catch (PDOException $e) {
            echo "<div class='alert alert-danger'>
    <strong>Erro: </strong>Ao inserir os dados para a galeria</div>";
            mail("heitor.aguia@gmail.com", "Erro ao salvar na galeria", $e->getMessage());
        }

    }

    // Pesquisa de fotos por Categorias.
    public function pesquisaCategoria(ControleDeDados $pesquisar){

        try{

         $pdo = Conexao::conectar();

        
        // Pesquisa imagens por categorias e links para ver fotos inteiras. 
        $stmt = $pdo->prepare("SELECT NOME_FOTO,TITULO,ALBUM FROM fotos WHERE ALBUM = ? ORDER BY ALBUM");
        $stmt->bindValue(1, $pesquisar->getAlbum());
        $stmt->execute();
        if ($stmt->rowCount() > 0) {
            
         while ($foto = $stmt->fetch(PDO::FETCH_OBJ)) {

                 echo '<div class="col-md-4">';
                 echo '<a href="album/' . $foto->ALBUM . '/' . $foto->NOME_FOTO . '" target="_black" </a>';
                 echo '<img src="album/' . $foto->ALBUM . '/' . $foto->NOME_FOTO . '" class="img-rounded" />';
                 echo '<p id="titulo">' . $foto->TITULO . '</p></div>';

         }        
         
}else{
    echo "<div class='alert alert-info'>
    <strong>Não possui fotos no momento</strong></div>";
}

}       catch(PDOException $e){
            echo "<div class='alert alert-danger'>
    <strong>Erro: </strong>Na busca de imagens</div>";
    mail("heitor.aguia@gmail.com", "Erro na busca de imagens", $e->getMessage());
        }

        unset($pesquisar,$pdo,$stmt,$result,$numTotal,$qt,$totalPagina,$exibir,$pagina,$anterior,$posterior);
    }

    // Busca fotos adicionado por stella por categorias.
    public function buscafoto(){

        try {

             $pdo = Conexao::conectar();

            /* Busca número do album e passa via GET, e busca de outra tabela nome e a imagem da capa */
            $buscarFoto = $pdo->query("SELECT DISTINCT fotos.ALBUM,capas.CAPA,capas.NOME_CAPA FROM fotos JOIN capas ON fotos.ALBUM = capas.ID ORDER BY NOME_CAPA");
            // Verifica se possui algum registro.
            if ($buscarFoto->rowCount() > 0) {

            while ($foto = $buscarFoto->fetch(PDO::FETCH_OBJ)) {                

                echo '<div class="col-md-6 portfolio-item">';
                echo '<a href="galeriaImg.php?albuns='. $foto->ALBUM .'">';
                echo '<img src="album/capa/'. $foto->CAPA .'" title="'. $foto->NOME_CAPA .'" class="img-rounded" /></a>';
                echo '<h3 id="text-galeria"><a href="galeriaImg.php?albuns='. $foto->ALBUM .'">'. $foto->NOME_CAPA . '</a></h3></div>';

            }
           } else{
                echo '<div class="alert alert-info">
    <strong>Esta página não possui galerias no momento</strong></div>';
            }

            $buscarFoto->closeCursor();

            unset($buscarFoto,$foto,$pdo);

            
        } catch (PDOException $e) {
            echo "<div class='alert alert-danger'>
    <strong>Erro: </strong>Na busca de categorias de imagens</div>";
    mail("heitor.aguia@gmail.com", "Erro na busca de album", $e->getMessage());
        }

    }

    // Método de excluir imagens.
    public function excluirImagem(ControleDeDados $imagem){

        try {

             $pdo = Conexao::conectar();
            // Excluir imagens de duas tabelas (fotos e capas) com ID passado.
            $excluirFoto = $pdo->prepare("DELETE FROM fotos WHERE ID = ?");
            $excluirFoto->bindValue(1, $imagem->getId());
            $excluirFoto->execute();
            $excluirFoto->closeCursor();

            header("Location: http://www.artebeleza.esy.es/principal-galeria");
        
               // excluir do servidor.
               @unlink("../album/" . $imagem->getAlbum() . "/" . $imagem->getImagem());

            unset($pdo,$excluirFoto,$imagem);

        } catch (PDOException $e) {
            echo "<div class='alert alert-danger'>
    <strong>Erro: </strong>Ao excluir imagem</div>";
    mail("heitor.aguia@gmail.com", "Erro de exclusão de imagens", $e->getMessage());
        }

    }

    // Busca fotos adicionado por stella para exclusão.
    public function buscafotoExclusao(){

        try {

             $pdo = Conexao::conectar();
            // Busca imagens para excluir, busca de duas tabelas (FOTOS E CAPAS).
            $buscarFoto = $pdo->query("SELECT fotos.ID,fotos.NOME_FOTO,fotos.TITULO,fotos.ALBUM,capas.NOME_CAPA FROM fotos JOIN capas ON fotos.ALBUM = capas.ID");   
            // Verifica se possui imagens para excluir.
            if ($buscarFoto->rowCount() > 0) {     

            while ($foto = $buscarFoto->fetch(PDO::FETCH_OBJ)) {

                echo '<div class="col-md-4">';
                echo '<img src="album/' . htmlentities($foto->ALBUM) . '/' . htmlentities($foto->NOME_FOTO) . '" class="img-rounded" />';
                echo '<p id="titulo">' . $foto->TITULO . '</p>';
                echo '<td><a href="php/excluirFoto.php?id=' . htmlentities($foto->ID) . '&nomeAlbum=' . htmlentities($foto->ALBUM) . 
                '&nomeImagem=' . htmlentities($foto->NOME_FOTO) . '"><button type="button" class="btn btn-danger btn-sm">Excluir</button></a><p><p>';
                echo '</div>';
            }
}else{
    echo "<div class='alert alert-info'>
    <strong>Você não possui imagens para excluir</strong></div>";
}
            $buscarFoto->closeCursor();

            unset($buscarFoto,$foto,$pdo);

            
        } catch (PDOException $e) {
            echo "<div class='alert alert-danger'>
    <strong>Erro: </strong>Na busca de imagens</div>";
        }

    }

    // Select de capas de fotos
    public function albunsDeFotos(){

        try {

             $pdo = Conexao::conectar();

             $selectAlbum = $pdo->query("SELECT ID,NOME_CAPA FROM capas ORDER BY NOME_CAPA");

             while ($capas = $selectAlbum->fetch(PDO::FETCH_ASSOC)) {
               echo "<option value='{$capas['ID']}'> " . htmlentities($capas['NOME_CAPA']) . "</option>";    
             }

            $selectAlbum->closeCursor();

            unset($selectAlbum,$pdo,$capas);

            
        } catch (PDOException $e) {
            echo "<div class='alert alert-danger'>
    <strong>Erro: </strong>Ao selecionar os dados dos albuns</div>";
    mail("heitor.aguia@gmail.com", "Erro na seleção do album", $e->getMessage());
        }

    }

    // Pesquisa de acorde com o nome da capa

    public function pesquisarFotos($pesquisarImagem){

    try {

         $pdo = Conexao::conectar();

        $pesquisaDeImagem = $pdo->prepare("SELECT sum(fotos.TITULO),fotos.ALBUM,capas.CAPA,capas.NOME_CAPA FROM capas JOIN fotos ON capas.ID = fotos.ALBUM WHERE capas.NOME_CAPA LIKE ?");
        $pesquisaDeImagem->bindValue(1, '%' . $pesquisarImagem . '%');
        $pesquisaDeImagem->execute();

        $busca = $pesquisaDeImagem->rowCount();

        if ($busca >= 1) {
             echo "<div class='alert alert-success'>
            <strong>Foram encontado </strong>$busca album</div>";
            while ($foto = $pesquisaDeImagem->fetch(PDO::FETCH_OBJ)) {

                echo '<div class="col-md-4">';
                echo "<a href='galeriaImg.php?albuns=". $foto->ALBUM ."'>";
                echo "<img src='album/capa/". $foto->CAPA ."' title='". $foto->NOME_CAPA ."' class='img-rounded' /></a>";
                echo "<h3 id='text-galeria'><a href='galeriaImg.php?albuns=". $foto->ALBUM ."'>". $foto->NOME_CAPA ."</a></h3></div>";
            }
            
        }   else{
                echo "<div class='alert alert-danger'>
            <strong>Não foi encontrado nenhum album com as palavras (''$pesquisarImagem'')</strong></div>";
             
         }    
        
        unset($pesquisarImagem,$pesquisaDeImagem,$pdo,$busca,$foto);

                
            } catch (PDOException $e) {
                echo "<div class='alert alert-danger'>
    <strong>Erro: </strong>Ao realizar a pesquisa, entre em contato com o administrador do sistema</div>";
    mail("heitor.aguia@gmail.com", "Erro na pesquisa de imagem", $e->getMessage());
            }        

    }

    // Gravar capas de album.
    public function capas(ControleDeDados $capas){

        try {

             $pdo = Conexao::conectar();
             // busca se já possui a foto da capa e nome
             $buscarDados = $pdo->prepare("SELECT CAPA,NOME_CAPA FROM capas WHERE CAPA = ? OR NOME_CAPA = ?");
             $buscarDados->bindValue(1, $capas->getCapa());
             $buscarDados->bindValue(2, $capas->getTitulo());
             $buscarDados->execute();
             
            // Verifica se possui realmente o nome ou a foto.
             if ($buscarDados->rowCount() == 1) {
                echo '<div class="alert alert-danger">
             <strong>Atenção! </strong>Foto da capa ou o nome já existe gravado</div>';
            
            // Caso não existe insert na tabela.
             }else{

            $gravarCapas = $pdo->prepare("INSERT INTO capas (CAPA,NOME_CAPA) VALUES (?,?)");
            $gravarCapas->bindValue(1, $capas->getCapa());
            $gravarCapas->bindValue(2, $capas->getTitulo());

            if ($gravarCapas->execute()) {
               echo "<div class='alert alert-success'>
       <strong>Status: </strong>Gravado com sucesso</div>";
               $gravarCapas->closeCursor();
            }else{
                echo "<div class='alert alert-danger'>
    <strong>Atenção! </strong>Erro ao gravar,verifique se já possui o mesmo nome de capa</div>";
            }

        }
            
            unset($pdo,$gravarCapas,$capas,$buscarDados);
        } catch (PDOException $e) {
           echo "<div class='alert alert-danger'>
    <strong>Atenção! </strong>Ocorreu um erro no cadastro, entre em contato com o administrador do sistema</div>";
    mail("heitor.aguia@gmail.com", "Erro no cadastro de capas", $e->getMessage());
        }
    

    }

    public function excluirAlbum(ControleDeDados $excluirAlbum){

        try {

             $pdo = Conexao::conectar();

            $excluirAlbumSelecionado = $pdo->prepare("DELETE FROM capas WHERE ID = ?");
            $excluirAlbumSelecionado->bindValue(1, $excluirAlbum->getAlbum());
            
             if ($excluirAlbumSelecionado->execute()) {
                echo "<div class='alert alert-success'>
    <strong>Status: </strong>Album excluido com sucesso</div>";  
                    // seleciona todos os tipos de foto.
                    $galeria = "album/" . $excluirAlbum->getAlbum() . "/" . "*.*";
                   // excluir todos os arquivos que estão dentro da pasta.
                    @array_map("unlink", glob($galeria));
                    // exclui a pasta desejada.
                    @rmdir("album/". $excluirAlbum->getAlbum());

            }else{
                echo "<div class='alert alert-danger'>
    <strong>Atenção! </strong>Ocorreu um erro na exclusão do album, entre em contato com o administrador do sistema</div>";
            }

            unset($excluirAlbum,$pdo,$galeria,$excluirAlbumSelecionado);
            
        } catch (PDOException $e) {
            echo "<div class='alert alert-danger'>
    <strong>Atenção! </strong>Ocorreu um erro na exclusão dos albuns, entre em contato com o administrador do sistema</div>";
    mail("heitor.aguia@gmail.com", "Erro na exclusão de albuns", $e->getMessage());
        }

    }

    public function updateSenha($senha,$id){

        try {
             $pdo = Conexao::conectar();

            $recupera = $pdo->prepare("UPDATE usuario SET SENHA = ? WHERE ID_USUARIO = ?");
            $recupera->bindValue(1, $senha);
            $recupera->bindValue(2, $id);
            $recupera->execute();

            unset($recupera,$pdo,$senha,$id);

        } catch (PDOException $e) {
           mail("heitor.aguia@gmail.com", "Erro na atualização da senha", $e->getMessage() . " Function updateSenha");
            echo "<div class='alert alert-danger'>
    <strong>Atenção! </strong>Ocorreu um erro ao tentar enviar os dados, entre em contato com o administrador do sistema</div>";
         
        }

    }

    public function recuperarSenha(ControleDeDados $recuperarSenha){

        try {
            
             $pdo = Conexao::conectar();

            $recuperacaoDeSenha = $pdo->prepare("SELECT ID_USUARIO,NOME,EMAIL,CPF FROM usuario WHERE EMAIL = ? AND CPF = ? LIMIT 3");
            $recuperacaoDeSenha->bindValue(1, $recuperarSenha->getEmail());
            $recuperacaoDeSenha->bindValue(2, $recuperarSenha->getCpf());
            $recuperacaoDeSenha->execute();
            
            $ids = $recuperacaoDeSenha->fetch(PDO::FETCH_OBJ);
            
            if ($recuperacaoDeSenha->rowCount() > 0) {

            $id = (int) $ids->ID_USUARIO;
            $nome = $ids->NOME;
            // Caracteres para senha.
            $caracteres = "0123456789abcdefghijklmnopqrstuvwxyz";
            // Sorteio de 8 Caracteres em letra maiúscula.
            $senhaGerada = substr(str_shuffle(strtoupper($caracteres)),0,9);
          
            $senhaEnviar = $senhaGerada;

            $this->updateSenha(sha1($senhaGerada),$id);

            mail($recuperarSenha->getEmail(), "Recuperação de Senha", "Olá " . $nome . " \nSua nova senha foi gerada com sucesso. Segue os passos para logar no sistema.\nLogin: " . $this->getEmail() . "\nSenha nova: " . $senhaEnviar . "\nCaso não tenha solicitado, entre em contato imediatamente com o administrador do sistema");
            echo "<div class='alert alert-success'>
    <strong>Aguarde o recebimento do e-mail com seu login e senha</strong></div>";

            }else{
                echo "<div class='alert alert-danger'>
    <strong>E-Mail ou Cpf são invalidos</strong></div>";
            }

            unset($pdo,$senhaGerada,$caracteres,$recuperacaoDeSenha,$senhaEnviar,$id,$ids,$nome);

        } catch (PDOException $e) {
           mail("heitor.aguia@gmail.com", "Erro na atualização da senha", $e->getMessage());
            echo "<div class='alert alert-danger'>
    <strong>Atenção! </strong>Ocorreu um erro ao tentar enviar os dados, entre em contato com o administrador do sistema</div>";
        }

    }

    
    public function trocarSenha($senhaAntiga,$senhaNova){
        
        try {
            
             $pdo = Conexao::conectar();
            
            $buscarSenha = $pdo->prepare("SELECT ID_USUARIO,SENHA FROM usuario WHERE SENHA = ?");
            $buscarSenha->bindValue(1, $senhaAntiga);
            $buscarSenha->execute();
            $recuperar = $buscarSenha->fetch(PDO::FETCH_OBJ);
            if ($buscarSenha->rowCount() == 1) {
                
                $id = (int)$recuperar->ID_USUARIO;
                
                $this->updateSenha(sha1($senhaNova),$id);

                echo '<div class="alert alert-success">
                     <strong>Status: </strong>A sua senha foi alterada com sucesso</div>';
                
            }else{
                 echo '<div class="alert alert-danger">
                     <strong>Atenção! </strong>Senha digitada está incorreta</div>';
            }
            
            unset($pdo,$senhaAntiga,$senhaNova,$recuperar,$id);
            
        } catch (PDOException $exc) {
            mail("heitor.aguia@gmail.com", "Erro na atualização da senha", $exc->getMessage() . " Function trocarSenha");
            echo "<div class='alert alert-danger'>
    <strong>Atenção! </strong>Ocorreu um erro ao tentar enviar os dados, entre em contato com o administrador do sistema</div>";
        }
            
    }

    public function buscarValores(){

        try {

            $pdo = Conexao::conectar();

            $buscarValores = $pdo->query("SELECT ID_VALORES,SERVICOS  FROM valores ORDER BY SERVICOS");

            while($valores = $buscarValores->fetch(PDO::FETCH_ASSOC)){

                echo '<option value="' . $valores['ID_VALORES'] . '">' . $valores['SERVICOS'] . '</option>';

            }

            unset($pdo,$valores,$buscarValores);

        } catch (PDOException $e) {
            echo '<div class="alert alert-danger">
    <strong>Atenção! </strong>Ocorreu um erro ao tentar buscar os dados do serviço</div>';
    mail("heitor.aguia@gmail.com", "Erro na busca de Serviços", $e->getMessage());
        }

    }

    public function editarFotos(ControleDeDados $editarImagem){

        try {

            $pdo = Conexao::conectar();

            $editaImagem = $pdo->prepare();
            $editaImagem->bindValue(1, $editarImagem->getImagem());
            $editaImagem->execute();

            $editaImagem->closeCursor();

            unset($editaImagem,$editarImagem,$pdo);

            
        } catch (PDOException $e) {
            echo 'erro ' . $e->getMessage();
        }


    }


}
