<?php
require_once 'UsuarioDAO.php';
if (!isset($_SESSION)) {
    session_start();
}
?>
<?php

// excluir usuário

if (@$_SESSION['nome']):
    if (isset($_GET['id'])):

        $id = (int) htmlEntities(trim($_GET['id']));

        $excluir = new Usuario();
        $excluir->setId($id);
        $excluir->excluir($excluir);

        unset($id,$excluir);

    else:

        echo '<div class="alert alert-danger">
    <strong>Erro ao excluir.</strong><br> Entre em contato com o administrador do sistema</div>';
    unset($_GET['id'],$_SESSION['nome']);
    endif;

else:
     unset($_SESSION['nome']);
     header("Location: ../");
     exit();

endif;
