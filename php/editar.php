<?php
session_start();
require_once 'UsuarioDAO.php';
require_once 'Conexao.php';

if (!isset($_SESSION)) {
    session_start();
}

if (isset($_POST['ok'])):
    if (@$_SESSION['nome']):

        if (isset($_GET['id'])):

            $editar = new Usuario();

            $data = filter_input(INPUT_POST, "txtData", FILTER_SANITIZE_MAGIC_QUOTES);
            $hora = filter_input(INPUT_POST, "txtHora", FILTER_SANITIZE_MAGIC_QUOTES);
            $id = (int) htmlEntities(trim($_GET['id']));
            $dataRevertida = implode("-", array_reverse(explode("/", $data)));

            $editar->setData(trim($dataRevertida));
            $editar->setHora(trim($hora));
            $editar->setId(trim($id));

            $editar->editar($editar);

            unset($data, $hora, $id, $dataRevertida, $editar);

        else:

            echo '<div class="alert alert-danger">
    <strong>Erro ao editar.</strong><br> Entre em contato com o administrador do sistema</div>';

        endif;

    else:
        header("Location: ../");
    endif;

endif;
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" type="image/png" href="../imagens/studio.png">
        <link rel="stylesheet" href="http://code.jquery.com/ui/1.9.0/themes/base/jquery-ui.css" />
        <script src="http://code.jquery.com/jquery-1.8.2.js"></script>
        <script src="http://code.jquery.com/ui/1.9.0/jquery-ui.js"></script>
        <script src="../javascript/jquery.maskedinput.min.js"></script>
        <script src="../javascript/jquery.inputmask.js"></script>
        <script src="../javascript/validacao.js"></script>

        <title>Editar Registro</title>

        <!-- CSS -->
        <link href="../css/bootstrap.min.css" rel="stylesheet">

        <!-- IE10 desktop Windows 8 -->
        <link href="../css/ie10-viewport-bug-workaround.css" rel="stylesheet">

        <link href="../css/jumbotron.css" rel="stylesheet">

        <script src="../css/ie-emulation-modes-warning.js"></script>

        <style>
             h4{
                font-family: "Times New Roman", Times, serif;
                color: #23527c;
            }   

            h1{
                text-align: center; 
                font-family: "Times New Roman", Times, serif;

            }

            #texto{
                font-family: "Times New Roman", Times, serif;
                color: #5a8393;
            }
        </style>

        <script>
        // Voltar para a página anterior.
        function voltarPagina() {
         window.history.go(-1);
        }
        </script>

        <script>
            $(function () {
                $("#data").datepicker({
                    dateFormat: 'dd/mm/yy',
                    dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado', 'Domingo'],
                    dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S', 'D'],
                    dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb', 'Dom'],
                    monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
                    monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez']
                });
            });
        </script>

    </head>

    <body>

        <!-- Menu -->
        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container">

                <a class="navbar-brand" href="http://www.artebeleza.esy.es/principal">Stella Gomes</a>
            </div>

        </nav>    

        <div class="jumbotron">
            <h1><?php

            $pdo = Conexao::conectar();

            $r = $pdo->prepare("SELECT NOME_CLIENTE FROM cadastro WHERE ID_CADASTRO = ?");
            $r->bindValue(1, $_GET['id']);

            $r->execute();

            while ($result = $r->fetch(PDO::FETCH_OBJ)) {
                echo "Cliente: " . $result->NOME_CLIENTE;
            }

            unset($pdo,$r,$result);


             ?></h1>
            <div class="container">
                <h2 id="texto">Editar Cadastro</h2>
                
                <form method="POST">

                    <div class="form-group">
                        <label for="data">
                            <h4>*Data de Agendamento</h4>
                        </label>
                        <input type="text" class="form-control" name="txtData" id="data" placeholder="Agendar a data">
                    </div> 

                    <div class="form-group">
                        <label for="pwd"><h4>*Hora</h4></label>
                        <input type="time" class="form-control" name="txtHora" id="pwd" placeholder="Digite o horário">
                    </div>

                    <button type="submit" class="btn btn-success" name="ok">Gravar</button>
                    <button type="reset" class="btn btn-danger">Limpar</button>
                    <button onclick="voltarPagina()" class="btn btn-info">Voltar</button>
                </form>
            </div>
        </div>
    </body>
</html>
