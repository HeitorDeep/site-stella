<?php
require_once dirname(__FILE__) . "/ControleDeDados.php";
interface ControleInterface {
   
    public function logar(ControleDeDados $dados);
    public function cadastrar(ControleDeDados $cadastrar);
    public function excluir(ControleDeDados $excluir);
    public function editar(ControleDeDados $editar);
    public function checkBrute($id);
    public function buscaDados();
    public function buscaSelect();
    public function indentificacao(ControleDeDados $indentificacao);
    public function galeria(ControleDeDados $imagem);
    public function buscafoto();
    public function excluirImagem(ControleDeDados $imagem);
    public function buscafotoExclusao();
    public function pesquisaCategoria(ControleDeDados $pesquisar);
    public function capas(ControleDeDados $capas);
    public function excluirAlbum(ControleDeDados $excluirAlbum);
    public function recuperarSenha(ControleDeDados $recuperarSenha);
    public function albunsDeFotos();
    public function pesquisarFotos($pesquisarImagem);
    public function updateSenha($senha,$id);
    public function editarFotos(ControleDeDados $editarImagem);
 
}
