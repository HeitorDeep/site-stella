<?php

class  ControleDeDados {
   
    // Atributos
    private $login;
    private $senha;
    private $nome;
    private $telefone;
    private $celular;
    private $endereco;
    private $data;
    private $hora;
    private $id;
    private $ip;
    private $hostName;
    private $titulo;
    private $imagem;
    private $resultado;
    private $album;
    private $capa;
    private $email;
    private $cpf;
    private $acompanhantes;
    

    // Getters e Setters
    function getLogin() {
        return $this->login;
    }

    function getSenha() {
        return $this->senha;
    }

    function getNome() {
        return $this->nome;
    }

    function getTelefone() {
        return $this->telefone;
    }

    function getCelular() {
        return $this->celular;
    }

    function getEndereco() {
        return $this->endereco;
    }

    function getData() {
        return $this->data;
    }

    function getHora() {
        return $this->hora;
    }

    function getHena() {
        return $this->hena;
    }

    function getSimples() {
        return $this->simples;
    }

    function getId() {
        return $this->id;
    }

    function getIp() {
        return $this->ip;
    }

    function getHostName() {
        return $this->hostName;
    }

    function getTitulo() {
        return $this->titulo;
    }

    function getImagem() {
        return $this->imagem;
    }

    function getResultado() {
        return $this->resultado;
    }

    function getCapa(){
        return $this->capa;
    }

    function getAlbum() {
        return $this->album;
    }

    function getEmail() {
        return $this->email;
    }

    function getCpf() {
        return $this->cpf;
    }

    function getAcompanhante(){
        return $this->acompanhantes;
    }

    function setLogin($login) {
        $this->login = $login;
    }

    function setSenha($senha) {
        $this->senha = sha1($senha);
    }

    function setNome($nome) {
        $this->nome = trim(ucwords($nome));
    }

    function setTelefone($telefone) {
    
        if ($telefone == null) {
            $this->telefone = "Não Possui";
        }else{
            $this->telefone = trim($telefone);
        }

    }

    function setCelular($celular) {
        $this->celular = trim($celular);
    }

    function setEndereco($endereco) {
        $this->endereco = trim(ucwords($endereco));
    }

    function setData($data) {
        $this->data = $data;
    }

    function setHora($hora) {
        $this->hora = $hora;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setIp($ip) {
        $this->ip = $ip;
    }

    function setHostName($hostName) {
        $this->hostName = $hostName;
    }    
    
    function setTitulo($titulo) {
        $this->titulo = ucwords(trim($titulo));
    } 

    function setImagem($imagem) {
        $this->imagem = $imagem;
    } 

    function setResultado($resultado) {
        $this->resultado = $resultado;
    }

    function setCapa($capa){
        $this->capa = $capa;
    }

    function setAlbum($album) {
        $this->album = $album;
    }
    
    function setEmail($email) {
        $this->email = trim($email);
    }

    function setCpf($cpf) {
        $this->cpf = trim($cpf);
    }
    
    function setAcompanhante($acompanhantes){
        $this->acompanhantes = $acompanhantes;
    }

}
