<?php
/*
  Classe de conexão ao banco de dados.
 */
class Conexao{
    # Variável que guarda a conexão PDO.
    protected static $pdo;
    # Private construct - garante que a classe só possa ser instanciada internamente.
    private function __construct(){
        # Informações sobre o banco de dados:
        $pdo_host = "localhost";
        $pdo_nome = "stella";
        $pdo_usuario = "root";
        $pdo_senha = "";
        $pdo_driver = "mysql";
  
        try{
            # Atribui o objeto PDO à variável $pdo.
            self::$pdo = new PDO("$pdo_driver:host=$pdo_host; dbname=$pdo_nome", $pdo_usuario, $pdo_senha);
            # Garante que o PDO lance exceções durante erros.
            self::$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        
        }catch (PDOException $e){
            
            # Então não carrega nada mais da página.
           mail("heitor.aguia@gmail.com", "Erro na base de dados", $e->getMessage());
             die("<div class='alert alert-danger'>
             <strong>Erro na conexão com a base de dados</strong></div>");
        }
    }
    # Método estático - acessível sem instanciação.
    public static function conectar(){
        # Garante uma única instância. Se não existe uma conexão, criamos uma nova.
        if (!self::$pdo){
            new Conexao();
        }
        # Retorna a conexão.
        return self::$pdo;
    }
}