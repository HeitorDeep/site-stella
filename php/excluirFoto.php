<?php
require_once 'UsuarioDAO.php';
if (!isset($_SESSION)) {
    session_start();
}
?>
<?php

// excluir usuário

if (@$_SESSION['nome']):

    if (isset($_GET['id']) && isset($_GET['nomeImagem']) && isset($_GET['nomeAlbum'])):

        $id = (int) htmlEntities(trim($_GET['id']));
        $imagem = htmlEntities(trim($_GET['nomeImagem']));
        $album = (int) htmlentities($_GET['nomeAlbum']);

        $excluir = new Usuario();
        $excluir->setAlbum($album);
        $excluir->setImagem($imagem);
        $excluir->setId($id);
        $excluir->excluirImagem($excluir);

        unset($id,$excluir,$imagem,$album);

    else:

        echo '<div class="alert alert-danger">
    <strong>Erro ao excluir.</strong><br> Entre em contato com o administrador do sistema</div>';
    unset($_GET['id'],$_SESSION['nome']);
    endif;

else:
     unset($_SESSION['nome']);
     header("Location: http://www.artebeleza.esy.es/");
     exit();

endif;
