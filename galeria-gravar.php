<?php
require_once "php/UsuarioDAO.php";
if (!isset($_SESSION)) {
    session_start();
}
// tempo de inatividade e verificação de acesso ao arquivo
$tempo = new Usuario();
$tempo->tempo();

if (isset($_POST['ok'])) {

    $dados = new Usuario();
    // Pega dados em array.

 $titulos = filter_input(INPUT_POST, 'tituloFoto', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
 $imagem = filter_var($_FILES['foto'], FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
 $tipoAlbum = filter_input(INPUT_POST, 'album', FILTER_SANITIZE_MAGIC_QUOTES);

 // Extensão da imagem.
 $extensao = array('.jpg','.jpeg','.png','.gif','.bmp');
 
      // Verifica se os campos foram nulos.
    if (empty($titulos) || empty($imagem)) {
        echo '<div class="alert alert-danger">
        <strong>Atenção! </strong>Os campos não podem ser vazios</div>';
    }
   
   else{

          // Busca os dados em array, obtendo titulo da foto e a imagem.
          foreach (@array_combine($titulos, $imagem['name']) as $nomeTitulo => $fotos) {
            
                  // Verifica o tipo do arquivo.
                  if (in_array(strrchr($fotos, '.'), $extensao)) {
                    
                      // Pega os dados e joga no setters.
                          $img = utf8_decode($fotos);
                          $title = utf8_decode($nomeTitulo);

                          $dados->setImagem($img);
                          $dados->setTitulo($title);
                          $dados->setAlbum($tipoAlbum);
                          // Grava os dados.
                          $dados->galeria($dados);
                         
                  }else{
                  echo '<div class="alert alert-danger">
                     <strong>Atenção! </strong>Você só pode gravar imagem do tipo <strong>jpg, jpeg, png, gif e bmp</strong></div>';
                }
                 
          }
             
          // Busca os dados para o envio da imagem ao servidor.
          for ($i = 0; $i < count($imagem['name']); $i++) { 

            // Verifica se o arquivo já existe no servidor.
            if (!file_exists("album/".$dados->getAlbum()."/".$imagem['name'][$i])) {

              // Verifica se existe a pasta no servidor.
                if (is_dir("album/".$tipoAlbum)) {
                  // Move para o servidor
                  move_uploaded_file($imagem['tmp_name'][$i], "album/".$dados->getAlbum()."/".$imagem['name'][$i]);
                 
                }else{
                 // Cria a pasta caso não exista no servidor.
                  @mkdir("album/".$tipoAlbum);
                  move_uploaded_file($imagem['tmp_name'][$i], "album/".$dados->getAlbum()."/".$imagem['name'][$i]);
                }
                 
                  
            }

          }
}

}
    // Limpa as variaveis.
    unset($imagem,$tipoAlbum,$titulos,$dados,$img,$title,$nomeTitulo,$foto,$i);
    

?>

<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" type="image/png" href="imagens/studio.png">
        <link rel="stylesheet" href="css/jquery-ui.css" />
        <link rel="shortcut icon" type="image/png" href="imagens/studio.png">
        <script src="javascript/jquery-1.8.2.js"></script>
        <script src="javascript/jquery-ui.js"></script>
        <script src="javascript/jquery.min.js"></script>

        <script>
        // Verificação de campos vazios
            function galeriaFoto(){
             if (document.formulario.tituloFoto.value == "" || document.formulario.foto.value == "" || document.formulario.albuns.value == "") {
                alert("Os campos não podem ser nulos");
                return false;
                }
            }
        </script>
        <script>
            //tecla ENTER
            $(document).ready(function(){
                /* ao pressionar uma tecla em um campo que seja de class="inputUnico", 
                em vez de submeter o formulário, vai pular para o próximo campo.
                o formulário só vai executar o submit com a tecla enter se estiver no ultimo campo do formulário*/
                $('.form-control').keypress(function(e){
                    /* 
                     * verifica se o evento é Keycode (para IE e outros browsers)
                     * se não for pega o evento Which (Firefox)
                    */
                   var tecla = (e.keyCode?e.keyCode:e.which);
                    
                   /* verifica se a tecla pressionada é a tecla ENTER */
                   if(tecla == 13){
                       /* guarda o seletor do campo onde foi pressionado Enter */
                       campo =  $('.form-control');
                       /* pega o indice do elemento*/
                       indice = campo.index(this);
                       /*soma mais um ao indice e verifica se não é null
                        *se não for é porque existe outro elemento
                       */
                      if(campo[indice+1] != null){
                         /* adiciona mais 1 no valor do indice */
                         proximo = campo[indice + 1];
                         /* passa o foco para o proximo elemento */
                         proximo.focus();
                      }else{
                        return true;
                      }
                   }
                   if(tecla == 13){
                    /* impede o submit caso esteja dentro de um form */
                    e.preventDefault(e);
                    return false;
                    }else{
                    /* se não for tecla enter deixa escrever */
                    return true;
                    }
                })
             })

            $(document).ready(function() {
  var max_fields = 10; //maximum input boxes allowed
  var wrapper = $(".input_fields_wrap"); //Fields wrapper
  var add_button = $(".add_field_button"); //Add button ID

  var x = 1; //initlal text box count
  $(add_button).click(function(e) { //on add input button click
    e.preventDefault();
    var length = wrapper.find("input:text").length;

    if (x < max_fields) { //max input box allowed
      x++; //text box increment
      $(wrapper).append('<div class="form-group"><label for="txtReserva'+(length+1)+'">Campo Extra'+(length+1)+'<input type="text" id="txtReserva'+(length+1)+'" class="form-control" name="tituloFoto[]" /></label><a href="#" class="remove_field">Remover Campo</a></div>'); //add input box
    }
  
  });

  $(wrapper).on("click", ".remove_field", function(e) { //user click on remove text
    e.preventDefault();
    $(this).parent('div').remove();
    x--;
  })

});

        </script>

        <title>Cadastro da Galeria</title>

        <!-- CSS -->
      <!-- CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- IE10 desktop Windows 8 -->
        <link href="css/ie10-viewport-bug-workaround.css" rel="stylesheet">

        <link href="css/jumbotron.css" rel="stylesheet">
        <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
        <script src="css/ie-emulation-modes-warning.js"></script>

         <!--[if lt IE 9]>
         <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
         <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
         <![endif]-->

        <style>

            h4{
                font-family: "Times New Roman", Times, serif;
                color: #23527c;
            }

            #texto-imagem{
                font-family: "Times New Roman", Times, serif;
                color: #5a8393;
            }

            #albuns{
                width: 100%;
                height: 30px;
                border-radius: 5px;
            }

            strong{
                color: #23527c;
            }

        </style>

    </head>

    <body>

        <!-- Menu -->
        <nav class="navbar navbar-inverse navbar-fixed-top" >

            <div class="container">
                <div class="navbar-header">
                 <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <a href="http://www.artebeleza.esy.es/Logout"><img src="imagens/logout.png" width="20" height="20" alt="logout"/></a>
                    </button>
                    <a class="navbar-brand" href="http://www.artebeleza.esy.es/principal">Principal</a>
                    <a class="navbar-brand" href="http://www.artebeleza.esy.es/galeria">Galeria</a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <div class="form-group">
                        <div class="navbar-form navbar-right">   
                            <a href="http://www.artebeleza.esy.es/Logout"><img src="imagens/logout.png" width="32" height="32" alt="logout"/></a>  
                        </div>
                    </div>
               </div>
            </div>
        </nav>



        <div class="jumbotron">
            <div class="container">

            <h2 id="texto-imagem">Salvar imagens</h2>

                <!-- Formulário de Galeria -->
                <form method="POST" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" name="formulario" enctype="multipart/form-data" onsubmit="return galeriaFoto()">
        
                <div class="form-group">
                <div class="input_fields_wrap">
  <button class="add_field_button btn btn-success">Adicionar mais Botões</button>
  <div>
                        <label for="tituloFoto"><h4>*Titulo da Foto</h4></label>
                        <input type="text" class="form-control" name="tituloFoto[]" id="tituloFoto" placeholder="Digite o Titulo" />
                </div>
               
                <div class="form-group">
                        <label for="foto"><h4>*Foto</h4></label>
                        <input type="file" class="form-control" name="foto[]" id="foto" multiple="multiple" />

                </div>

                <div class="form-group">

                 <h4>Categoria do Album</h4>
                <strong>Selecionar pasta existente</strong>
                <select name="album" id="albuns">

                <?php 
                    
                // Objeto do método de buscar nome de albuns.    
                $buscaAlbuns = new Usuario();

                $buscaAlbuns->albunsDeFotos();

                unset($buscaAlbuns);

                ?>    

                </select>
                </div>

                        <button type="submit" name="ok" class="btn btn-success" >Cadastrar</button>

                        <button type="reset" class="btn btn-danger">Limpar</button>

             </form>
             <br/>
             <br/>
             
            <a href="http://www.artebeleza.esy.es/cadastroAlbum" class="btn btn-info">Cadastrar Albuns</a>
            <a href="http://www.artebeleza.esy.es/editarFoto" class="btn btn-success">Editar Imagem</a><br/><br/>
            <a href="http://www.artebeleza.esy.es/excluirAlbum" class="btn btn-primary">Excluir Album</a>
            </div>  
            
        

    </body>
</html>