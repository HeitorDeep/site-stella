<?php
require_once "php/UsuarioDAO.php";

if (isset($_POST['ok'])){

    $dado = new Usuario();

    $email = filter_input(INPUT_POST, "email", FILTER_SANITIZE_MAGIC_QUOTES);
    $cpf = filter_input(INPUT_POST, "cpf", FILTER_SANITIZE_MAGIC_QUOTES);

    $dado->setEmail($email);
    $dado->setCpf($cpf);

    $dado->recuperarSenha($dado);

    unset($email,$cpf,$dado);

}

    
?>

<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" type="image/png" href="../imagens/studio.png">
        <link rel="stylesheet" href="http://code.jquery.com/ui/1.9.0/themes/base/jquery-ui.css" />
        <script src="javascript/jquery-1.8.2.js"></script>
        <script src="javascript/jquery-ui.js"></script>
        <script src="javascript/jquery.maskedinput.min.js"></script>
        <script src="javascript/jquery.inputmask.js"></script>
        <script src="javascript/validacao.js"></script>

        <title>Recuperação de Senha</title>

        <!-- CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- IE10 desktop Windows 8 -->
        <link href="css/ie10-viewport-bug-workaround.css" rel="stylesheet">

        <link href="css/jumbotron.css" rel="stylesheet">

        <script src="css/ie-emulation-modes-warning.js"></script>

        <style>
            h3{
                font-family: "Times New Roman", Times, serif;
                color: #23527c;
            }

            h4{
                font-family: "Times New Roman", Times, serif;
                color: #23527c;
            }

            select {
                width: 100%;
                padding: 8px;
                margin: 8px 0;
                border: 1px solid #ccc;
                -webkit-border-radius: 5px;
                -moz-border-radius: 5px;
                border-radius: 5px;
            }
            

        </style>


        <script>
            $(function () {
    
                $("#cpf").mask("999.999.999-99");
                
            });
        </script>
        <script>
           
           function validacao() {
    if (document.form.email.value == "" || document.form.cpf.value == "") {
        alert("Por favor, Prencher os campos corretamente para efetuar a recuperação de senha");
        return false;
    }
    
}

        </script>
    </head>

    <body>

        <!-- Menu -->
        <nav class="navbar navbar-inverse navbar-fixed-top">

            <div class="container">
                <div class="navbar-header">
                    <a class="navbar-brand" href="http://www.artebeleza.esy.es/esqueciSenha">Recuperação de Senha</a>
                    <a class="navbar-brand" href="http://www.artebeleza.esy.es/">Voltar</a>
                </div>

            </div>
        </nav>

        <!-- Inicio de apresentação -->
        <div class="jumbotron">
            <div class="container">

                <!-- Formulário de cadastro -->
                <form method="POST" name="form" onsubmit="return validacao()">
                    <center><h3>Recuperação de Senha</h3></center>
                    
                    <div class="form-group">
                        <label for="email"><h4>*E-mail</h4></label>
                        <input type="email" class="form-control" name="email" id="email" placeholder="Digite seu email" required />
                    </div>

                    <div class="form-group">
                        <label for="cpf"><h4>*Cpf</h4></label>
                        <input type="text" class="form-control" name="cpf" id="cpf" placeholder="Digite seu cpf" required />
                    </div>
                    
                        <button type="submit" name="ok" class="btn btn-success">Enviar</button>
                        <button type="reset" class="btn btn-danger">Limpar</button>
                        
                </form>
                    
            </div>  
        </div>

    </body>
</html>         