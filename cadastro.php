<?php
require_once "php/UsuarioDAO.php";

if (!isset($_SESSION)) {
    session_start();
}

if (isset($_POST['ok'])):
    $dado = new Usuario();

    // Pega as informações
    $nome = filter_input(INPUT_POST, "txtNome", FILTER_SANITIZE_MAGIC_QUOTES);
    $telefone = filter_input(INPUT_POST, "txtTelefone", FILTER_SANITIZE_MAGIC_QUOTES);
    $trabalhos = filter_input(INPUT_POST,'servico', FILTER_SANITIZE_MAGIC_QUOTES);
    $endereco = filter_input(INPUT_POST, "txtEndereco", FILTER_SANITIZE_MAGIC_QUOTES);
    $data = filter_input(INPUT_POST, "txtData", FILTER_SANITIZE_MAGIC_QUOTES);
    $celular = filter_input(INPUT_POST, "txtCelular", FILTER_SANITIZE_MAGIC_QUOTES);
    $hora = filter_input(INPUT_POST, "hora", FILTER_SANITIZE_MAGIC_QUOTES);
    $qtdAcompanhantes = filter_input(INPUT_POST, "numeros", FILTER_SANITIZE_MAGIC_QUOTES);


if ($nome != null && $trabalhos != null && $endereco != null && $data != null && $celular != null && $telefone != null && $qtdAcompanhantes != null) {
    setcookie("nome", $nome, time() + 18000);
    setcookie("trabalhos", $trabalhos, time() + 18000);
    setcookie("endereco", $endereco, time() + 18000);
    setcookie("data", $data, time() + 18000);
    setcookie("celular", $celular, time() + 18000);
    setcookie("telefone", $telefone, time() + 18000);
    setcookie("qtdAcompanhantes", $qtdAcompanhantes, time() + 18000);
}

    // Array de horas para verificação.
    $testarHoras = array('08:00:00','09:30:00','10:30:00','11:30:00','12:30:00','14:30:00','15:30:00','16:30:00','17:30:00','18:30:00');

    // Converter data para o Banco de dados.
    $dataRevertida = implode("-", array_reverse(explode("/", $data)));

    // Verifica se os campos de horas estão corretos
    if (in_array($hora, $testarHoras)):

            // Setters dos dados.   
            $dado->setResultado($trabalhos);  
            $dado->setAcompanhante($qtdAcompanhantes);      
            $dado->setNome($nome);
            $dado->setTelefone($telefone);
            $dado->setCelular($celular);
            $dado->setEndereco($endereco);
            $dado->setData($dataRevertida);
            $dado->setHora($hora);  

            // Método para cadastrar 
            $dado->cadastrar($dado);         

    
    else:

        // adiciona o ip e indentificação do computador do individuo que tente burlar o sistema
        $ip = getenv("REMOTE_ADDR");
        $hostname = gethostbyaddr($_SERVER['REMOTE_ADDR']);

        $dado->setIp($ip);
        $dado->setHostName($hostname);

        $dado->indentificacao($dado);

         echo "<div class='alert alert-danger'>
    <strong>Atenção! </strong>A hora solicitada foi alterada indevidamente pelo usuário, por favor atualize a página e tente novamente</div>";

    endif;
    
    unset($nome,$telefone,$endereco,$data,$celular,$hora,$dataRevertida,$dado,$ip,$hostname,$verificacao,$soma,$acomp);

endif;
?>

<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="Fernando Heitor">
        <link rel="shortcut icon" type="image/png" href="imagens/studio.png">
        <link rel="stylesheet" href="http://code.jquery.com/ui/1.9.0/themes/base/jquery-ui.css" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="javascript/validacao.js"></script>
      

        <title>Agendamento Arte Beleza</title>

        <!-- CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- IE10 desktop Windows 8 -->
        <link href="css/ie10-viewport-bug-workaround.css" rel="stylesheet">

        <link href="css/jumbotron.css" rel="stylesheet">

        <script src="css/ie-emulation-modes-warning.js"></script>

        <style>
            h3{
                font-family: "Times New Roman", Times, serif;
                color: #23527c;
            }

            h4{
                font-family: "Times New Roman", Times, serif;
                color: #23527c;
            }

            select {
                width: 100%;
                padding: 8px;
                margin: 8px 0;
                border: 1px solid #ccc;
                -webkit-border-radius: 5px;
                -moz-border-radius: 5px;
                border-radius: 5px;
            }
            

        </style>


        <script>
            $(function () {
                $("#data").datepicker({
                    dateFormat: 'dd/mm/yy',
                    dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado', 'Domingo'],
                    dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S', 'D'],
                    dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb', 'Dom'],
                    monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
                    monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez']
                });
            });
            $(function () {
                $("#telefone").mask("(99) 9999-9999");
                $("#celular").mask("(99) 9999-99999");
                $("#data").mask("99/99/9999");
            });

            
        </script>
        
        <script src="javascript/jquery-1.8.2.js"></script>
        <script src="javascript/jquery-ui.js"></script>
        <script src="javascript/jquery.maskedinput.min.js"></script>
        <script src="javascript/jquery.inputmask.js"></script>
        <script src="javascript/apresentacao.js"></script>
        
    </head>

    <body>

        <!-- Menu -->
        <nav class="navbar navbar-inverse navbar-fixed-top">

            <div class="container">
                <div class="navbar-header">
                    <a class="navbar-brand" href="http://www.artebeleza.esy.es/cadastro">Agendamento</a>
                </div>

            </div>
        </nav>

        <!-- Inicio de apresentação -->
        <div class="jumbotron">
            <div class="container">

                <!-- Formulário de cadastro -->
                <form method="POST" name="form" enctype="multipart/form-data" onsubmit="return validacao()">
                    <center><h3>Agende seu dia <img src="imagens/calendario.png" width="40px" height="30px" alt="calendario"/></h3></center>
                    <center><h3>Atendimento de Seg á Sex</h3>
                        <h3>08:00 ás 12:00 e 14:00 ás 18:30</h3></center>
                    <div class="form-group">
                        <label for="nome"><h4>*Nome</h4></label>
<input type="text" class="form-control" name="txtNome" id="nome" placeholder="Digite seu nome" value="<?php if(isset($_COOKIE['nome'])){echo $_COOKIE['nome'];} else {echo "";} ?>" >
                    </div>

                    <div class="form-group">
                        <label for="telefone"><h4>Telefone</h4></label>
<input type="text" class="form-control" name="txtTelefone" id="telefone" placeholder="Digite seu telefone" value="<?php if(isset($_COOKIE['telefone'])){echo $_COOKIE['telefone'];} else {echo "";} ?>">
                    </div>


                    <div class="form-group">
                        <label for="celular"><h4>*Celular</h4></label>
                        <input type="text" class="form-control" name="txtCelular" id="celular" placeholder="Digite seu WhatsApp" value="<?php if(isset($_COOKIE['celular'])){echo $_COOKIE['celular'];} else {echo "";} ?>">
                    </div>      

                    <div class="form-group">
                        <label for="endereco"><h4>*Endereço</h4></label>
                        <input type="text" class="form-control" name="txtEndereco" id="endereco" placeholder="Digite seu endereço" value="<?php if(isset($_COOKIE['endereco'])){echo $_COOKIE['endereco'];} else {echo "";} ?>">
                    </div>      

                    <div class="form-group">
                        <label for="data"><h4>*Data de Agendamento</h4></label>
                        <input type="text" class="form-control" name="txtData" id="data" placeholder="Agende sua data" value="<?php if(isset($_COOKIE['data'])){echo $_COOKIE['data'];} else {echo "";} ?>">
                    </div> 

                    <fieldset>

                        <h4>*Horário</h4>

                        <select name="hora">
                 <?php 
                 
                 // Busca dos dados do select (Horas).
                  $buscarDadosDoSelect = new Usuario();

                  $buscarDadosDoSelect->buscaSelect();

                  unset($buscaSelect);

                 ?>
                           
                        </select>

                    </fieldset>
                        <fieldset>
                        <h4>*Serviços</h4>
                
                        <select name="servico">

                         <?php

                          $buscarDadosDoSelect->buscarValores();

                          unset($buscarDadosDoSelect);


                          ?>

                        </select>
                    </fieldset><br/>

                       <!-- <button type="submit" name="ok" class="btn btn-success">Cadastrar</button> -->
                       <button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal">Cadastrar</button>

                        <button type="reset" class="btn btn-danger">Limpar</button>

                        <!-- Modal -->
                         <div class="modal fade" id="myModal" role="dialog">
                             <div class="modal-dialog">
    
                                  <!-- Modal content-->
                                <div class="modal-content">
                                  <div class="modal-header">
                                     <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Quantidade de Acompanhante</h4>
                             </div>
                        <div class="modal-body">
                                <p>Trabalhamos até 16 acompanhantes</p>
                        <input type="number" min="0" max="16" name="numeros" required value="<?php if(isset($_COOKIE['qtdAcompanhantes'])){echo $_COOKIE['qtdAcompanhantes'];} else {echo "";} ?>"/>
                        </div>
                    <div class="modal-footer">
                        <button type="submit" name="ok" class="btn btn-success">Cadastrar</button>
                    </div>
                        </div>
                        
                </form>
                    
            </div>  
        </div>

    </body>
</html>         