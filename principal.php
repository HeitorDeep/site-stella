<?php
require_once 'php/UsuarioDAO.php';
if (!isset($_SESSION)):
    session_start();
endif;
?>
<?php
// tempo de inatividade e verificação de acesso ao arquivo
$tempo = new Usuario();
$tempo->tempo();

?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" type="image/png" href="imagens/studio.png">

        <title>Principal</title>

        <!-- CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- IE10 desktop Windows 8 -->
        <link href="css/ie10-viewport-bug-workaround.css" rel="stylesheet">

        <link href="css/jumbotron.css" rel="stylesheet">

        <script src="css/ie-emulation-modes-warning.js"></script>

        <style>
            
            body{
                background: #eee;
            }

            h1{
                text-align: center; 
                margin-bottom: 60px;
                font-family: mv boli,sans-serif,arial;
            }
            table{
                text-align: center;
                font-family: cursive,sans-serif,arial;
                
            }

            th{
                font-size: 18pt;
                text-align: center;
                color: #3a798c;  
            }
            td:hover{
                background-color: #269abc;
            }
            /* Apresentação do nome do usuário */
            #nome{
                 font-size: 48px;
            }

            /* Botões de excluir e editar */
            #botao:hover{
                background-color: white;
            }
                       
             /* Resultados da consultas */
            .resultado{
                font-size: 15px;
                font-family: sans-serif,arial,mv boli;
            }

            .pagination{
                margin-top: 415px; 
                margin-left: 31%;
                position: absolute;
            }

        </style>


    </head>

    <body>

        <!-- Menu -->
        <nav class="navbar navbar-inverse navbar-fixed-top">

            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <a href="http://www.artebeleza.esy.es/Logout"><img src="imagens/logout.png" width="20" height="20" alt="logout"/></a>
                    </button>
                    <a class="navbar-brand" href="http://www.artebeleza.esy.es/principal">Principal</a>
                    <a class="navbar-brand" href="http://www.artebeleza.esy.es/trocaSenha">Trocar Senha</a>
                </div>

                <div id="navbar" class="navbar-collapse collapse">
                    <div class="form-group">
                        <div class="navbar-form navbar-right">   
                            <a href="http://www.artebeleza.esy.es/Logout"><img src="imagens/logout.png" width="32" height="32" alt="logout"/></a>  
                        </div>
                    </div>
                </div>
            </div>
        </nav>

        <!-- Inicio de apresentação -->

            <div class="container">

                <h1 id="nome"><?php echo htmlentities($_SESSION['nome']); ?></h1>

            <!-- Tabela de dados -->
            <div class="table-responsive">
                        <table class="table">
                    <thead>
                        <tr>
                            <th>Nome</th>
                            <th>Data</th>
                            <th>Hora</th>
                            <th>Endereço</th>
                            <th>Telefone</th>
                            <th>Celular</th>
                            <th>Serviços</th>
                            <th>Acompanhante</th>
                            <th>Total</th>
                        </tr>
                    </thead>

                    <tbody>

                        <?php
                        
                        // Consulta dos dados agendados.
                        $buscaDados = new Usuario();

                        $buscaDados->buscaDados();

                        unset($buscaDados);

                        ?>  

                    </tbody>
                </table>
            </div>
            <a href="http://www.artebeleza.esy.es/galeria"><button class="btn btn-info btn-sm">Galeria</button></a>
            <a href="http://www.artebeleza.esy.es/galeria-gravar"><button class="btn btn-success btn-sm">Gravar Imagem</button></a>
            <a href="http://www.artebeleza.esy.es/principal-galeria"><button class="btn btn-danger btn-sm">Excluir Imagem</button></a>
            </div>
        
    </body>
</html>