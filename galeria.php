<?php
require_once 'php/UsuarioDAO.php';

if (isset($_GET['ok'])) {
 
    $pesquisa = new Usuario();

    $busca = filter_input(INPUT_GET, "pesquisa", FILTER_SANITIZE_MAGIC_QUOTES);

    $pesquisa->pesquisarFotos($busca);

    unset($pesquisa,$busca);

}

?>
<!DOCTYPE html>
<html lang="pt-br">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" type="image/png" href="imagens/studio.png">

    <title>Galeria Arte Beleza</title>

    <!-- CSS -->
    <link href="css/2-col-portfolio.css" rel="stylesheet">

    <!-- HTML5 Shim  -->
   
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<style>
/*Controle de tamanho da imagem*/
img{
    width: 100%;
    height: 369px;
    margin-left: auto;
}
/*Passar o mouse em cima do link*/
a:hover{
    background: #a6b3d1;
    border-radius: 5px;
    text-decoration:none;
}
/*Link clicado*/
a:visited{
    color: #6d9bba;
    text-decoration: none;
}

</style>
<!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
</head>

<?php flush(); ?>

<body>

    <!-- Navegação -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Menu -->
            <div class="navbar-header">
                <a class="navbar-brand" href="http://www.artebeleza.esy.es/">Arte Beleza</a>
            </div>
         
         </div>
        
    </nav>

    <!-- Principal conteúdo -->
    <div class="container">

        <div class="row">
            <div class="col-lg-12">
            <!-- Titulo -->
                <h1 class="page-header">Galeria
                    <small>Trabalhos Realizados</small>
                </h1>
<form method="get">
     <div class="input-group">
            <input type="text" class="form-control" name="pesquisa" placeholder="Pesquisar">
        <div class="input-group-btn">
            <button class="btn btn-default" name="ok" type="submit"><i class="glyphicon glyphicon-search"></i></button>
        </div>
    </div>
</form> 

<p>
            </div>

        </div>

        <!-- Imagens -->
        <div class="row">
        
<?php

 $buscarFotos = new Usuario();

 $buscarFotos->buscafoto();

 unset($buscarFotos);

?>

    </div>
    
     <!-- jQuery -->
    <script src="javascript/jquery.js"></script>

    <!-- Bootstrap -->
    <script src="javascript/bootstrap.min.js"></script>

</body>

</html>
