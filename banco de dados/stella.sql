-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 08-Abr-2017 às 04:59
-- Versão do servidor: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `stella`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `cadastro`
--

CREATE TABLE `cadastro` (
  `ID_CADASTRO` int(11) NOT NULL,
  `DATA` date NOT NULL,
  `HORA` time NOT NULL,
  `NOME_CLIENTE` varchar(35) CHARACTER SET latin1 NOT NULL,
  `TELEFONE` char(14) CHARACTER SET latin1 NOT NULL,
  `CELULAR` char(15) CHARACTER SET latin1 NOT NULL,
  `ENDERECO` varchar(50) NOT NULL,
  `ACOMPANHANTE` varchar(2) NOT NULL,
  `RESULTADO` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Estrutura da tabela `capas`
--

CREATE TABLE `capas` (
  `ID` int(11) NOT NULL,
  `CAPA` varchar(255) CHARACTER SET latin1 NOT NULL,
  `NOME_CAPA` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Estrutura da tabela `fotos`
--

CREATE TABLE `fotos` (
  `ID` int(11) NOT NULL,
  `NOME_FOTO` varchar(255) CHARACTER SET latin1 NOT NULL,
  `TITULO` varchar(40) CHARACTER SET utf8 NOT NULL,
  `ALBUM` varchar(25) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Estrutura da tabela `hora`
--

CREATE TABLE `hora` (
  `ID` int(11) NOT NULL,
  `HORA` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `hora`
--

INSERT INTO `hora` (`ID`, `HORA`) VALUES
(1, '08:00:00'),
(2, '09:30:00'),
(3, '10:30:00'),
(4, '11:30:00'),
(5, '12:30:00'),
(6, '14:30:00'),
(7, '15:30:00'),
(8, '16:30:00'),
(9, '17:30:00'),
(10, '18:30:00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tentativas`
--

CREATE TABLE `tentativas` (
  `ID` int(10) UNSIGNED NOT NULL,
  `ID_USUARIO` int(11) NOT NULL,
  `TEMPO` varchar(15) CHARACTER SET latin1 NOT NULL,
  `IP` varchar(15) CHARACTER SET latin1 NOT NULL,
  `HOST_NAME` varchar(30) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

CREATE TABLE `usuario` (
  `ID_USUARIO` int(11) NOT NULL,
  `NOME` varchar(30) CHARACTER SET latin1 NOT NULL,
  `EMAIL` varchar(30) CHARACTER SET latin1 NOT NULL,
  `SENHA` varchar(60) CHARACTER SET latin1 NOT NULL,
  `CPF` char(14) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`ID_USUARIO`, `NOME`, `EMAIL`, `SENHA`, `CPF`) VALUES
(1, 'Stella', 'stellagomes06@gmail.com', 'b0f44571644f9ea3c4440bb803853a4dda25237e', '385.831.808-65');

-- --------------------------------------------------------

--
-- Estrutura da tabela `valores`
--

CREATE TABLE `valores` (
  `ID_VALORES` int(11) NOT NULL,
  `VALORES` decimal(5,2) NOT NULL,
  `SERVICOS` varchar(70) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `valores`
--

INSERT INTO `valores` (`ID_VALORES`, `VALORES`, `SERVICOS`) VALUES
(1, '15.00', 'Design de sobrancelha simples (R$ 15,00)'),
(2, '20.00', 'Design de sobrancelha com aplicação de hena (R$ 20,00)'),
(3, '100.00', 'Maquiagem Individual(R$ 100,00)'),
(4, '30.00', 'Noivinha (R$ 30,00)'),
(5, '90.00', 'Debutante (R$ 90,00)'),
(6, '175.00', 'Noiva/Debutante + Acompanhante R$ 175,00 (R$ 75,00 cada Acompanhante)');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cadastro`
--
ALTER TABLE `cadastro`
  ADD PRIMARY KEY (`ID_CADASTRO`);

--
-- Indexes for table `capas`
--
ALTER TABLE `capas`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `NOME_CAPA` (`NOME_CAPA`),
  ADD UNIQUE KEY `CAPA` (`CAPA`);

--
-- Indexes for table `fotos`
--
ALTER TABLE `fotos`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `hora`
--
ALTER TABLE `hora`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tentativas`
--
ALTER TABLE `tentativas`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`ID_USUARIO`),
  ADD UNIQUE KEY `EMAIL` (`EMAIL`),
  ADD UNIQUE KEY `CPF` (`CPF`);

--
-- Indexes for table `valores`
--
ALTER TABLE `valores`
  ADD PRIMARY KEY (`ID_VALORES`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cadastro`
--
ALTER TABLE `cadastro`
  MODIFY `ID_CADASTRO` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `capas`
--
ALTER TABLE `capas`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `fotos`
--
ALTER TABLE `fotos`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `hora`
--
ALTER TABLE `hora`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `tentativas`
--
ALTER TABLE `tentativas`
  MODIFY `ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `usuario`
--
ALTER TABLE `usuario`
  MODIFY `ID_USUARIO` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `valores`
--
ALTER TABLE `valores`
  MODIFY `ID_VALORES` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
