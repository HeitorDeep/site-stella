<?php
require_once 'php/UsuarioDAO.php';

if (isset($_GET['albuns'])) {

    $galeria = new Usuario();
    $imagem = htmlentities(trim($_GET['albuns']));
    @$galeria->setAlbum($imagem);
    
    unset($imagem);

}else{
    echo "<div class='alert alert-warning'>
    <strong>Não foi passado nenhuma informação para busca de fotos</strong></div>";
}

?>
<!DOCTYPE html>
<html lang="pt-br">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" type="image/png" href="imagens/studio.png">   

    <title>Galeria Arte Beleza</title>

    <!-- HTML5 Shim  -->
   
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<style>
/*Controle de tamanho da imagem*/
img{
    width: 100%;
    height: 369px;
    margin-left: auto;
}
/*Passar o mouse em cima do link*/
a:hover{
    background: #a6b3d1;
    border-radius: 5px;
    text-decoration:none;
}
/*Link clicado*/
a:visited{
    color: #6d9bba;
    text-decoration: none;
}

.alert-info{
    margin-top: -20px;
}

</style>
    
  <script src="javascript/jquery-1.8.2.js"></script>
  <!-- CSS -->
    <link href="css/2-col-portfolio.css" rel="stylesheet">  
   <!-- Bootstrap --> 
    <link href="css/bootstrap.min.css" rel="stylesheet">

</head>

<?php flush(); ?>

<body>


    <!-- Navegação -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Menu -->
            <div class="navbar-header">
                <a class="navbar-brand" href="http://www.artebeleza.esy.es/">Arte Beleza</a>
            </div>
         
         </div>
        
    </nav>

    <!-- Principal conteúdo -->
    <div class="container">

        <div class="row">
            <div class="col-lg-12">
            <!-- Titulo -->
                <h1 class="page-header">
                    <small>Arte Beleza</small>
                </h1>
            </div>
        </div>

        <!-- Imagens -->
        <div class="row">
        
<?php
// Método de select de galeria selecionada
@$galeria->pesquisaCategoria($galeria);
unset($galeria);

?>
    </div>
    
     <!-- jQuery -->
    <script src="javascript/jquery.js"></script>

    <!-- Bootstrap -->
    <script src="javascript/bootstrap.min.js"></script>

</body>

</html>
